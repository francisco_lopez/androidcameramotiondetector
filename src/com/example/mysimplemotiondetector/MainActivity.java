package com.example.mysimplemotiondetector;

import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class MainActivity extends Activity {

	private boolean capturePictures = false;
	private Camera mCamera = null;
	private Button button;
	private SurfacePreview view;
	public byte[] data0 = null;
	public byte[] data1 = null;
	private Context context;
	private PreviewCallback mPreviewCallback = new PreviewCallback() {
		


		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			if (data == null) return;
		}
	};
	
	private PictureCallback mPictureCallback = new PictureCallback() {
		
		// callback function from camera
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			// do the processing in another thread to keep the App responsive
			new DetectMotion().execute(data, data0, data1);
			// update the values
			data0 = data1;
			data1 = data;
		}
	};
	

	private void captureNextPicture() {
		if (capturePictures) {
			if (mCamera == null) {
				mCamera = Camera.open();
				view = new SurfacePreview(this, mCamera);
				try {
					mCamera.setPreviewDisplay(view.getHolder());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			mCamera.startPreview();
			mCamera.takePicture(null, null, mPictureCallback);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.button);

	}

	@Override
	protected void onPause() {
		super.onPause();
		// Because the Camera object is a shared resource, it's very
		// important to release it when the activity is paused.
		if (mCamera != null) {
			mCamera.release();
			mCamera = null;
		}
		capturePictures = false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		context = getApplicationContext();
		
		// Open the default i.e. the first rear facing camera.
		mCamera = Camera.open();
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (capturePictures) {
					capturePictures = false;
				} else {
					capturePictures = true;
					captureNextPicture();
				}
			}
		});
		mCamera.setPreviewCallback(mPreviewCallback);

		BaseLoaderCallback MyOpenCVCallBack = new BaseLoaderCallback(this) {
			@Override
			public void onManagerConnected(int status) {
				switch (status) {
				case LoaderCallbackInterface.SUCCESS: {
					// Then we are good to go
				}
					break;
				default: {
					super.onManagerConnected(status);
				}
					break;
				}
			}
		};
		if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this, MyOpenCVCallBack)) {
			// Then we are out of luck
		}
	}

	

	// small helper function to convert the byte array to a Mat object that
	// OpenCV uses
	private Mat convertToMat(byte[] data) {
		Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
		Mat tmp = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8UC1);
		Utils.bitmapToMat(bitmap, tmp);
		return tmp;
	}

	// This is the core functionality of the motion detection algorithm
	private double isMotionDetected(byte[] data1, byte[] data2, byte[] data3) {
		Mat mat1 = convertToMat(data1);
		Mat mat2 = convertToMat(data2);
		Mat mat3 = convertToMat(data3);
		Mat d1 = new Mat(mat1.width(), mat1.height(), mat1.type());
		Mat d2 = new Mat(mat1.width(), mat1.height(), mat1.type());
		Mat result = new Mat(mat1.width(), mat1.height(), mat1.type());

		Core.absdiff(mat2, mat1, d1);
		Core.absdiff(mat3, mat1, d2);
		Core.bitwise_and(d1, d2, result);
		// You can play around with the threshold value 35
		Imgproc.threshold(result, result, 35, 255, Imgproc.THRESH_BINARY);

		double value = Core.norm(result);
		return value;
	}

	private class DetectMotion extends AsyncTask<byte[], Void, Void> {

		

		@Override
		protected Void doInBackground(byte[]... params) {
			byte[] data0 = params[0];
			byte[] data1 = params[1];
			byte[] data2 = params[2];
			if (data1 != null && data2 != null) {
				// You can play around with the threshold value 10000.0
				if (isMotionDetected(data0, data1, data2) > 10000.0) {
					
					final Bitmap bitmap = BitmapFactory.decodeByteArray(data0, 0, data0.length);
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							ImageView imageView = (ImageView) findViewById(R.id.imdageView);
							imageView.setImageBitmap(bitmap);
							Log.i("DetectMotion", "isMotionDetected");
						}
					});
				}
			}
			try {
				// you need some interval here, otherwise the camera cannot
				// follow and will throw an exception
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			captureNextPicture();
		}

	}



}
